module.exports = {
  dev: {
    PORT: 3000,
    LOGTYPE: ':method :url :status :response-time ms - :res[content-length]',
    APPENTRY: '../build',
    DB: {
      URI: 'mongodb://localhost:27017/logueit'
    },
    SESSION: {
      secret: 'my-secret',
      saveUninitialized: false,
      resave: true
    },
    IOSESSION: { autoSave: true }
  },
  prod: {
    PORT: 3000,
    LOGTYPE: ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length]',
    APPENTRY: '../build',
    DB: {
      URI: 'mongodb://localhost:27017/logueit'
    },
    SESSION: {
      secret: 'my-secret',
      saveUninitialized: false,
      resave: true
    },
    IOSESSION: { autoSave: true }
  }
}