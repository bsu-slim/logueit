const PROD     = process.env.PROD ? true : false;
const CONFIG   = PROD ? require('../config').prod : require('../config').dev;
const DBURI    = CONFIG.DB.URI;

const mongoose = require('mongoose');

// -- make database connection
mongoose.connect(DBURI, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.set('useFindAndModify', false);


// -- add database log handlers
const db = mongoose.connection;
db.on('error', console.error.bind(console, '[DATABASE] connection error:'));
db.once('open', () => console.log('[DATABASE] Database connected Successfully'));

module.exports = db;