const mongoose = require('mongoose');
const config = require('../config');
const Prompt = require('./prompts');
const Topic = require('./topics');
const prompts = require('./prompts.json');
const topics = require('./topics.json');

const PROD      = process.env.PROD ? true : false;
const URI = PROD ? config.prod.DB.URI : config.dev.DB.URI;
const CONNECTION_OPTIONS = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

mongoose.connect(URI, CONNECTION_OPTIONS);

const db = mongoose.connection;
db.on('error', console.error.bind(console, '[DATABASE] connection error:'));
db.once('open', () => console.log('[DATABASE] Database connected Successfully'));

const loadDB = async () => {
  let created_prompts = await Prompt.loadData(prompts);
  let created_topics = await Topic.loadData(topics);

  console.log('[DATABASE] loaded prompts:');
  console.log(created_prompts);

  console.log('[DATABASE] loaded topics:');
  console.log(created_topics);

  mongoose.connection.close();
}

loadDB();