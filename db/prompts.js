const mongoose = require('mongoose');

const Prompt = new mongoose.Schema({
  'en-US': {
    type: String,
    required: [true, 'Please Provide an English translation']
  },
  'it-IT': {
    type: String,
  },
  'ja-JP': {
    type: String,
  },
  'es-ES': {
    type: String,
  },
});

Prompt.static('loadData', (prompts) => mongoose.model('Prompt').create(prompts));
Prompt.static('counter', () => mongoose.model('Prompt').countDocuments().exec());
Prompt.static('random', (count) => mongoose.model('Prompt').findOne().skip(Math.floor(Math.random()*count)).exec())

module.exports = mongoose.model('Prompt', Prompt);