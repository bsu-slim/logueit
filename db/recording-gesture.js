const mongoose = require('mongoose');

const RecordingGesture = new mongoose.Schema({
  recordingId: {
    type: String,
    require: [true, 'Please provide a recording id value for recording gesture']
  },
  from: {
    type: String,
    require: [true, 'Please provide a from id value for recording gesture']
  },
  received: {
    type: Date,
    default: new Date(0)
  },
  x: {
    type: Number,
    require: [true, 'Please provide an x value for recording gesture']
  },
  y: {
    type: Number,
    require: [true, 'Please provide a y value for recording gesture']
  }
});

module.exports = mongoose.model('RecordingGesture', RecordingGesture);