const mongoose = require('mongoose');

const RecordingMessage = new mongoose.Schema({
  recordingId: {
    type: String,
    require: [true, 'Please provide a recording id value for recording message']
  },
  from: {
    type: String,
    require: [true, 'Please provide a from id value for recording message']
  },
  message: {
    type: String,
    require: [true, 'Please provide a message value for recording message']
  },
  received: {
    type: Date,
    default: new Date(0)
  }
});

module.exports = mongoose.model('RecordingMessage', RecordingMessage);