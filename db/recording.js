const mongoose = require('mongoose');

const Recording = new mongoose.Schema({
  roomId: {
    type: String,
    require: [true, 'Please provide a room id for recording']
  },
  topicId: {
    type: String,
    require: [true, 'Please provide a topic id for recording']
  },
  promptId: {
    type: String,
    require: [true, 'Please provide a topic id for recording']
  },
  startTime: {
    type: Date,
    require: [true, 'Please provide a start time for recording']
  },
  endTime: {
    type: Date,
    default: new Date(0)
  }
});

Recording.static('setEndTime', (rid, endTime) =>
  mongoose.model('Recording')
    .findByIdAndUpdate(rid, {endTime: endTime}, {new: true})
);

module.exports = mongoose.model('Recording', Recording);