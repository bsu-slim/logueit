const mongoose = require('mongoose');

const Room = new mongoose.Schema({
  room: {
    type: String,
    unique: true,
    require: [true, 'Please provide a room name']
  }
});

Room.static('createRoom', (room) => mongoose.model('Room').create(room));
Room.static('getRoom', (room) => mongoose.model('Room').findOne({room}).exec());

module.exports = mongoose.model('Room', Room);