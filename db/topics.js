const mongoose = require('mongoose');

const Topic = new mongoose.Schema({
  topic: {
    type: String,
    require: [true, 'Please provide a topic']
  }
})

Topic.static('loadData', (topics) => mongoose.model('Topic').create(topics));
Topic.static('counter', () => mongoose.model('Topic').countDocuments().exec());
Topic.static('random', (count) => mongoose.model('Topic').findOne().skip(Math.floor(Math.random()*count)).exec())

module.exports = mongoose.model('Topic', Topic);