const mongoose = require('mongoose');
const PLMongoose = require('passport-local-mongoose');

const User = new mongoose.Schema({
  email: {
    type: String,
    match: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    required: [true, 'Please provide an email']
  },
  name: {
    first: {
      type: String,
      required: [true, 'Please provide a first name']
    },
    last: {
      type: String,
      required: [true, 'Please provide a last name']
    }
  }
});

User.virtual('fullname').get(function() {
  return `${this.name.first} ${this.name.last}`;
});

User.plugin(PLMongoose, {usernameField: 'email', usernameLowerCase: true});

module.exports = mongoose.model('User', User);