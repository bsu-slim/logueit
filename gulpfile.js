const gulp = require('gulp');
const run = require('gulp-run');
const nodemon = require('gulp-nodemon');
const gulpMultiProcess = require('gulp-multi-process');

gulp.task('server', () => nodemon({
  script: './server/server.js',
  watch: ['./server', './db'],
  env: {'PORT': 3000}
}));

gulp.task('server:peer', () => nodemon({
  script: './server/peer.js',
  watch: ['./server'],
  env: {'PORT': 3001}
}));

gulp.task('client', () => {
  gulp.src('./', { cwd: __dirname })
    .pipe(run('npm run build'))
});

gulp.task('servers', () => gulpMultiProcess(['server', 'server:peer']));

gulp.task('run', gulp.parallel(['servers', 'client']));