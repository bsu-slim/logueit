const passport  = require('passport');

let users = {};

passport.serializeUser(function(user, done) {
  console.log('user', user);
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  console.log('id', id )
  console.log(users)
  done(null, users[id]);
});

function verify(accessToken, refreshToken, profile, done) {
  // TODO: FIND OR CREATE ONE USER

  // lookup user
  let lookup = users[profile.id];
  
  // if no user make one
  if(!lookup) {
    users[profile.id] = profile;
    lookup = profile;
  }

  // return user
  done(null, lookup);

}

function isAuthenticated(req,res,next) {
  console.log(req.session);
  if(req.user)
     return next();
  else
     return res.status(401).json({
       error: 'User not authenticated'
     })

}

function success(req, res) {
  res.redirect('/');
}

function google(authopt) {
  return passport.authenticate('google', authopt);
}

module.exports = {
  verify: verify,
  success: success,
  google: google,
  isAuthenticated: isAuthenticated
}