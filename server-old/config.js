// follow https://developers.google.com/adwords/api/docs/guides/authentication#webapp
// to generate client id and secret
const GOOGLE_CLIENT_ID = '38543956475-mfvaok578kd5iiijdpr56gljq1h69tvh.apps.googleusercontent.com';
const GOOGLE_CLIENT_SECRET = 'YpIn9JjzU_6eE_ipBbQp5CGg';
module.exports = {
  port: 3000,
  db: {
    dbURI: "mongodb://localhost:27017/logueit"
  },
  auth: {
    google: {
      strategy: {
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: GOOGLE_CLIENT_SECRET,
        callbackURL: "http://localhost:3000/auth/google/cb",
        scope: "https://www.googleapis.com/auth/plus.login",
      },
      scope: { scope: ['email', 'profile'] },
      authopt: { 
        successRedirect: '/',
        failureRedirect: '/login'
      }
    }
  }
};