// follow https://developers.google.com/adwords/api/docs/guides/authentication#webapp
// to generate client id and secret
const GOOGLE_CLIENT_ID = '';
const GOOGLE_CLIENT_SECRET = '';
module.exports = {
  port: 3000,
  db: {
    dbURI: "serveraddrhere"
  },
  auth: {
    google: {
      strategy: {
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: GOOGLE_CLIENT_SECRET,
        callbackURL: "http://localhost:3000/auth/google/cb"
      },
      scope: { scope: ['https://www.googleapis.com/auth/plus.login'] },
      authopt: { failureRedirect: '/login'}
    }
  }
};