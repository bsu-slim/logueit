let mongoose = require('mongoose');
let config = require('../config');
let DBURI = config.db.dbURI;

mongoose.connect(DBURI, {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, '[DATABASE] connection error:'));

db.once('open', function() {
  console.log('[DATABASE] Database connected Successfully');
});

module.exports = mongoose.connection;