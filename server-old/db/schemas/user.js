const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');

const User = new mongoose.Schema({
  uid: String,
  username: {
    type: String,
    match: /^[a-z0-9_-]{3,16}$/,
    required: [true, 'Please provide a username']
  },
  password: {
    type: String,
    required: [true, 'Please provide a password']
  },
  name: {
    first: {
      type: String,
      required: [true, 'Please provide a first name']
    },
    last: {
      type: String,
      required: [true, 'Please provide a last name']
    }
  }
});

User.virtual('fullname').get(function() {
  return `${this.name.first} ${this.name.last}`;
});

User.plugin(passportLocalMongoose);

module.exports = User;