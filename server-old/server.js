// == DEPENDENCIES ==
// -- web server
const express    = require('express');
const bodyParser = require('body-parser');
const app        = express();
const http       = require('http').createServer(app);

// -- database
const db         = require('./db/connect');
const User       = require('./db/models/user');

// -- logging and file handling
const log        = require('morgan');
const path       = require('path');
const fs         = require('fs');
// const wav       = require('wav');

// -- sockets and sessions
const io         = require('socket.io')(http);
const esession   = require('express-session');
const isession   = require('express-socket.io-session');
const session    = esession(require('./session'));
const iosession  = isession(session, require('./session-io'));
const passport   = require('passport');
const lStrategy  = require('passport-local').Strategy;
const auth       = require('./auth');

// == CONFIG ==
// -- config deconstruction for neatness & graceful failure
const config    = require('./config');
const PORT      = process.env.PORT || config.port || 3000; // server port

// -- helper functions
function static(dir) { return path.join(__dirname, dir) }
const connectEnsureLogin = require('connect-ensure-login');

// -- dev logging
app.use(log('dev'));

// -- body parsing
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// -- production logging
/*app.use(log('dev', { // failure codes to console
  skip: function (req, res) { return res.statusCode < 400 }
}));*/
/*app.use(log('common', { // all logs to file
  stream: fs.createWriteStream(static('access.log'), { flags: 'a' })
}))*/

// == AUTH STRATEGY ==
// -- initialize passport
app.use(session); // use sessions (always before telling passport)
app.use(passport.initialize()); // init passport
app.use(passport.session()); // tell passport were using user sessions
passport.use(User.createStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// -- create routes for google auth round trip
// app.get('/auth/google', passport.authenticate('google', AUTHSCOPE)); // to google
// app.get('/auth/google/cb', auth.google(AUTHOPT)); // from google

// == STATIC FILES ==
// -- serve static login page
/*app.get('/login', (req, res) => {
  res.send('<a href="/auth/google">Sign In with Google</a>');
});*/

app.post('/login', (req, res, next) => {
  passport.authenticate('local',
  (err, user, info) => {
    if (err) {
      return next(err);
    }

    if (!user) {
      return res.redirect('/login?info=' + info);
    }

    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }

      return res.redirect('/');
    });

  })(req, res, next);
});

app.get('/login',
  (req, res) => res.sendFile('html/login.html',
  { root: __dirname })
);

// -- serve front end build
app.use('/', connectEnsureLogin.ensureLoggedIn(), express.static(static('../build')));

// -- catch any other route after everything above as part of react router route
app.get('*', connectEnsureLogin.ensureLoggedIn(), (req, res) => {
  //console.log(req.session);
  res.sendFile(static('../build/index.html')); // serve app
})

// == SOCKETS ==
// -- tell socket io we are using sessions
io.use(iosession);
// -- handle socket communications
io.on("connect", (socket) => { // on a new client connection
  console.log("New client connected"); // print connection
  
  socket.on('message', (message) => {
    if(message && message.replace(/\s/g, '') != '') {
      payload = { who: 'me', message: message};
      socket.emit('message', payload);
      payload.who = 'them';
      socket.broadcast.emit('message', payload);
    }
  });

  socket.on('disconnecting', () => {
    console.log("Client is disconnectiong...")
  });

  socket.on('disconnect', () => { // on client disconnect
    console.log("Client disconnected.");
  });

});
// == START SERVER ==
// -- listening on PORT for new connections
http.listen(PORT, () => { console.log(`listening on *:${PORT}`) });