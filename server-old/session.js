const esession  = require('express-session');
const { v4: uuidv4 } = require('uuid');
const sesstore  = require('session-file-store')(esession);
module.exports = {
  //genid: (req) => {
   // return uuidv4()
  //},
  //store: new sesstore({path : './sessions/'}),
  secret: 'my-secret',
  saveUninitialized: false,
  resave: false
};