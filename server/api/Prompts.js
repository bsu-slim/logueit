const Prompt = require('../../db/prompts');

let getRandomPrompt = (req, res) => {
  Prompt.count().exec((err, count) => {
    if(err) console.error(err);
    var rand = Math.floor(Math.random() * count);
    Prompt.findOne().skip(rand).exec((err, randomPrompt) => {
      res.json(randomPrompt);
    });
  })
}

let getRandomRoomPrompt = async () => {
  let count = await Prompt.counter();
  let random = await Prompt.random(count);
  return random;
}

module.exports = {
  get: {'random': getRandomPrompt},
  prompt: {'random': getRandomRoomPrompt }
}