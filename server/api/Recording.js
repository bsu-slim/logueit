const Recording = require('../../db/recording');

let create = async (recording) => {
  let rec = await Recording.create(recording);
  return rec;
};

let setEndTime = async (recid, ending) => {
  let rec = await Recording.setEndTime(recid, ending);
  return rec;
}

module.exports = {
  recording: { create, setEndTime }
}