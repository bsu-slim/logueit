const RecordingGesture = require('../../db/recording-gesture');

let create = async (recording) => {
  let gesture = await RecordingGesture.create(recording);
  return gesture;
}

module.exports = {
  gesture: { create }
}