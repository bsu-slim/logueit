const RecordingMessage = require('../../db/recording-message');

let create = async (recording) => {
  let message = await RecordingMessage.create(recording);
  return message;
}

module.exports = {
  message: { create }
}