const Room = require('../../db/room');

let getRoomOrCreate = async (room) => {
  let current = await Room.getRoom(room);
  if(!current) current = await Room.createRoom({room});
  return current;
};

module.exports = {
  room: { getRoomOrCreate }
}