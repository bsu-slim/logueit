const Topic = require('../../db/topics');

let getRandomTopic = (req, res) => {
  Topic.count().exec((err, count) => {
    if(err) console.error(err);
    var rand = Math.floor(Math.random() * count);
    Topic.findOne().skip(rand).exec((err, randomTopic) => {
      res.json(randomTopic);
    });
  })
}

let getRandomRoomTopic = async () => {
  let count = await Topic.counter();
  let random = await Topic.random(count);
  return random;
}

module.exports = {
  get: {'random': getRandomTopic},
  topic: {'random': getRandomRoomTopic}
}