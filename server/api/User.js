let getUser = (req, res) => res.json(req.user || {});

module.exports = {
  get: { 'get': getUser }
}