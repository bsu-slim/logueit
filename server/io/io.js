const io = require('socket.io');
const ss = require('socket.io-stream');


(socket) => {
  console.log("SOCKET New client connected"); // print connection

  
  const stream = ss.createStream();
  socket.on('testTrack', () => {
    const fp = path.resolve(__dirname, './test', './test-track.wav'); // file path
    const stat = fileSystem.statSync(filePath); // get file info
    const rstream = fileSystem.createReadStream(fp); // read stream
    rstream.pipe(stream) // read the audio file stream
    ss(socket).emit('trackStream', stream, { stat })
  })
  
  socket.on('message', (message) => {
    if(message && message.replace(/\s/g, '') != '') {
      payload = { who: 'me', message: message};
      socket.emit('message', payload);
      payload.who = 'them';
      socket.broadcast.emit('message', payload);
    }
  });

  socket.on('disconnecting', () => {
    console.log("SOCKET Client is disconnecting...")
  });

  socket.on('disconnect', () => { // on client disconnect
    console.log("SOCKET Client disconnected.");
  });

}

module.exports = iomain;