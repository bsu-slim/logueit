const passport = require('passport');

const login = (req, res, next) => {
  passport.authenticate('local',
  (err, user, info) => {
    if (err) {
      return next(err);
    }

    if (!user) {
      return res.redirect(`/${req.params.lang}/login?info=${info.message}`);
    }

    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }
      req.session.user = req.user;

      return res.redirect(`/${req.params.lang}/Dashboard`);
    });

  })(req, res, next);
}

const ensurelogin = (req, res, next) => {
  
  if (req.user) {
    next()
  } else {
    let loc = req.params[0] !== '/' ? req.params.lang : '/en-US';
    res.redirect(`/${loc}/login`)
  }
}

module.exports = {login: login, ensurelogin: ensurelogin};