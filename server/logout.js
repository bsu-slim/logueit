const logout = (req, res) => {
  req.session.user = null;
  req.logout();
  res.redirect(`/${req.params.lang}`);
};

module.exports = logout;