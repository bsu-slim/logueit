const { PeerServer } = require('peer');
const fs = require('fs');

//app.enable('trust proxy');

const PORT = process.env.PORT || 9000;
const SSL = {
  key: fs.readFileSync('./key.pem'),
  cert: fs.readFileSync('./cert.pem')
};

const peerServer = PeerServer({
  port: PORT,
  ssl: SSL
});