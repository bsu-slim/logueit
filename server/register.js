const User = require('../db/user');

const register = (req, res, next) => {
  console.log(req.body);
  User.register(new User({
    email: req.body.email,
    name: {
      first: req.body.fname,
      last: req.body.lname
    }
  }),
  req.body.password, 
  (err) => {
    if(err) {
      console.log('[DATABASE] error while registering user.', err)
      return next(err);
    }
    console.log('[DATABASE] user registered!');
    let loc = req.params[0] !== '/' ? req.params.lang : '/en-US';
    res.redirect(`/${loc}/login`);
  })
};

module.exports = register;