const path = require('path');
const fs   = require('fs');

// -- load config
const DEBUG     = false;
const PROD      = process.env.PROD ? true : false;
const CONFIG    = PROD ? require('../config').prod : require('../config').dev;
const LOGTYPE   = process.env.LOGTYPE  || CONFIG.LOGTYPE  || 'common';
const APPENTRY  = process.env.APPENTRY || CONFIG.APPENTRY || '../build';
const PORT      = process.env.PORT     || CONFIG.PORT     || 3000;
const IOSESSION = CONFIG.IOSESSION     || {autoSave: true};
const SESSION   = CONFIG.SESSION       || {
  secret: 'my-secret',
  saveUninitialized: false,
  resave: false
};

const SERVEROPTS = {
	key: fs.readFileSync('./key.pem'),
	cert: fs.readFileSync('./cert.pem')
};

// -- generated
const ENTRY = `${CONFIG.APPENTRY}/index.html`;

// -- web server
const express    = require('express');
const bodyParser = require('body-parser');
const app        = express();
const server     = require('https').createServer(SERVEROPTS, app);
const PeerServer = require('peer').ExpressPeerServer;
const ps         = PeerServer(server);

// -- logging
const log  = require('morgan');

// -- database
const db   = require('../db/connect');
const User = require('../db/user');

// -- api
const userAPI = require('./api/User');
const topicAPI = require('./api/Topics');
const promptAPI = require('./api/Prompts');
const roomAPI = require('./api/Room');
const recordingAPI = require('./api/Recording');
const gestureAPI = require('./api/RecordingGesture');
const messageAPI = require('./api/RecordingMessage');

// -- sockets & sessions
const io        = require('socket.io')(server);
const session   = require('express-session');
const isession  = require('express-socket.io-session');
const esession  = session(SESSION);
const passport  = require('passport');
const register  = require('./register');
const logout    = require('./logout');
const {
  login,
  ensurelogin
} = require('./login');

// -- helpers
const staticify  = dir => path.join(__dirname, dir);
const index      = (req, res) => res.sendFile(staticify(ENTRY));
const apiFactory = (location, api) => {
  let types = ['post', 'get', 'put', 'delete'];
  for(j = 0; j < types.length; j++) {
  let type = types[j];
  let endpoints = Object.keys(api[type] || {});
    for(let i = 0; i < endpoints.length; i++) {
      let endpoint = endpoints[i];
      console.log(`[SERVER] [FACTORY] ${type} /api${location}/${endpoint}`);
      app[type](
        `/api${location}/${endpoint}`, 
        PROD ? ensurelogin : (req, res, next) => {next()}, 
        api[type][endpoint]
      );

    }
  }
}

// -- logging
console.log(`[SERVER] starting in ${PROD ? 'prod': 'dev'} mode...`)
if(!PROD) {
  app.use(log(function (tokens, req, res) {
    return [
      '[SERVER]',
      `[${tokens.method(req, res)}]`,
      tokens.url(req, res),
      tokens.status(req, res),
      tokens.res(req, res, 'content-length'), '-',
      tokens['response-time'](req, res), 'ms'
    ].join(' ')
  }));
} else {
  app.use(log(LOGTYPE, {skip: (req, res) => { return res.statusCode < 400 }}));
  app.use(log(LOGTYPE, {stream: fs.createWriteStream(staticify('access.log'), { flags: 'a' })}));
}

app.use(esession);
io.use(isession(esession, IOSESSION));

// -- socket requests
let roomStates = {};
let callerLookup = {};

io.on('connect', client => {

  // client just connected
  console.log('[SOCKET] client connected');

  // deploy handlers to orchestrate WebRTC rooms
  // client is joining a room
  client.on('join', async (room, id) => {
    let user = client.handshake.session.user;                                    // get users session variable
    room = String(room);                                                         // make sure room is a string
    let name =`${user.name.first} ${user.name.last}`;                            // concat name together
    console.log(`[SOCKET] [${room}] [E] '${name}' is joining`);                    // print a nice server message
    client.join(room);                                                           // join io room
    client.callerid = id;                                                        // set a "phone number" for their current socket session
    callerLookup[id] = callerLookup[id] ? callerLookup[id] : {};                 // add caller to lookup table for graceful call disconnection
    callerLookup[id][room] = true;                                               // set the lookup key for the room
    let roomState = roomStates[room];                                            // try to get room from memory
    if (roomState) {                                                             // if room exists
      roomState.callers[id] = true;                                              // put joiner in the room
    } else {                                                                     // else if room does not exist, make one
      let dbroom = await roomAPI.room.getRoomOrCreate(room);                     // create new room in db
      roomState = {room: dbroom.room, id: dbroom._id};                           // in memory default room
      roomState.prompt = await promptAPI.prompt.random();                        // pick a random prompt
      roomState.topic = await topicAPI.topic.random();                           // pick a random topic
      roomState.callers = {};                                                    // create a dictionary of callers
      roomState.callers[id] = true;                                              // add self to caller dictionary
      roomStates[room] = roomState;                                              // store the in-memory room for later use
      let recording = await recordingAPI.recording.create({                      // create new recording automatically when someone joins the room
        roomId: roomState.id,
        topicId: roomState.topic._id,
        promptId: roomState.prompt._id,
        startTime: Date.now()
      });
      DEBUG && console.log('[SOCKET] [DEBUG] [ROOM]', dbroom);                   // print database objects for debug purposes
      DEBUG && console.log('[SOCKET] [DEBUG] [RECORDING]', recording);           // print database objects for debug purposes
      roomState.recording = recording;                                           // pass new recording to in-memory state
    }
    io.to(room).emit('joining', id);                                             // tell everyone were joining
    client.emit('joined', roomState);                                            // tell client its joining broadcast is complete and a room is ready for them
  });
  
  // client is sending a message in a room
  client.on('message', async (msg, room) => {
    let user = client.handshake.session.user;                                    // get users session variable
    room = String(room);                                                         // make sure room is a string
    let roomState = roomStates[room];                                            // get the room state for the message
    let name = `${user.name.first} ${user.name.last}`;                           // concat name together
    console.log(`[SOCKET] [${room}] [M] ${name}:`, msg);                         // print a nice server message
    if(msg && msg.replace(/\s/g, '') != '') {                                    // if there is a message payload and its not just spaces
      payload = { who: 'me', from: user, message: msg};                          // create a payload
      client.emit('message', payload);                                           // send it back to self for display
      payload.who = 'them';                                                      // modify the payload for them
      client.to(room).emit('message', payload);                                  // send the payload to the other clients in the room
      let message  = await messageAPI.message.create({
        recordingId: roomState.recording._id,
        from: user._id,
        message: msg,
        received: Date.now()
      });
      DEBUG && console.log('[SOCKET] [DEBUG] [ROOM]', message);                  // print database objects for debug purposes
    }
  });

  // client is gesturing in a room
  client.on('gesture', async (room, x, y) => {
    let user = client.handshake.session.user;                                    // get users session variable
    room = String(room);                                                         // make sure room is a string
    let roomState = roomStates[room];                                            // get the room state for the gesture
    let name =`${user.name.first} ${user.name.last}`;                            // concat name together
    console.log(`[SOCKET] [${room}] [G] ${name}: <${x}, ${y}>`);                 // print a nice server message
    client.to(room).emit('gesture', x, y)                                        // tell other clients in the room I gestured
    let gesture = await gestureAPI.gesture.create({                              // create a new gesture for curent recording
      recordingId: roomState.recording._id,
      from: user._id,
      received: Date.now(),
      x: x,
      y: y
    });
    DEBUG && console.log('[SOCKET] [DEBUG] [ROOM]', gesture);                    // print database objects for debug purposes

  })

  // client is leaving the room
  client.on('leave', async (room, id) => {
    let user = client.handshake.session.user;                                    // get users session variable
    room = String(room);                                                         // make sure room is a string
    let name =`${user.name.first} ${user.name.last}`;                            // concat name together
    console.log(`[SOCKET] [${room}] [E] '${name}' is leaving`);                  // print a nice server message
    client.leave(room);                                                          // remove client from the io room
    client.callerid = null;
    let roomState = roomStates[room];                                            // get the room the client is in
    delete roomState.callers[id];                                                // delete myself from the callers
    if (Object.keys(roomState.callers).length === 0) {                           // if room will be empty after client leaves
      let rid = roomState.recording._id;                                         // get the recording id
      let stopTime = new Date(Date.now());
      let updatedRecording = await recordingAPI.recording.setEndTime(            // update the recording id
        rid, stopTime
      );
      let dateStr = stopTime.toUTCString();                                      // get a human readable date
      console.log(`[SOCKET] [${room}] [E] empty! stopped at ${dateStr}`);        // print nice that the room is empty at end time
      DEBUG && console.log('[SOCKET] [DEBUG] [RECORDING]', updatedRecording);    // print database objects for debug purposes
      delete roomStates[room];                                                   // delete in-memory room, nobody is using it
      
    } else {                                                                     // if people still in room after client leaves
      io.to(room).emit('leaving', id);                                           // tell everyone I am leaving
    }
    client.emit('left', roomState);                                              // tell client its leaving broadcast is complete
  });

  // client closed its socket connection
  client.on('disconnect', async () => {
    let user = client.handshake.session.user;                                    // get users session variable
    if(client.callerid) {                                                        // if client has an active caller id
      let activecalls = Object.keys(callerLookup[client.callerid]);              // get all the clients calls
      let name =`${user.name.first} ${user.name.last}`;                            // concat name together
      for(let i = 0; i < activecalls.length; i++) {                              // for every call the client is is in
        let room = activecalls[i];                                               // get the call
        console.log(`[SOCKET] [${room}] [E] '${name}' disconnected`); // print nice disconnect message for each room they are in
        let roomState = roomStates[room];
        delete roomStates[room].callers[client.callerid];                        // remove client from the in-memmory call list
        if(Object.keys(roomStates[room].callers).length === 0) {                 // if client disconnecting makes the room empty
          let rid = roomState.recording._id;                                     // get the recording id
          let stopTime = new Date(Date.now());
          let updatedRecording = await recordingAPI.recording.setEndTime(        // update the recording id
            rid, stopTime
          );
          let dateStr = stopTime.toUTCString();                                  // get a human readable date
          delete roomStates[room];                                               // delete in-memory room, nobody is using it
          console.log(`[SOCKET] [${room}] [E] room empty! stopped at ${dateStr}`);    // print nice that the room is empty
          DEBUG && console.log('[SOCKET] [DEBUG] [RECORDING]', updatedRecording) // print database objects for debug purposes
        } else {                                                                 // if there are people left in the room
          io.to(room).emit('user-disconnected', client.callerid);                // tell everyone in the call that the callers connection dropped
        }
      }
    } else {                                                                     // if client has no active caller id
      console.log('[SOCKET] client disconnected');                               // print nice message that they disconnected with no callerid
    }
  });

});

// -- request preprocessing
app.use('/peer', ps);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// -- authentication
app.use(passport.initialize());
app.use(passport.session());
passport.use(User.createStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// -- rest requests
app.post('/:lang/login', login);
app.get( '/:lang/login', index);
app.post('/:lang/register', register);
app.get( '/:lang/register', index);
app.get( '/:lang/logout', logout);

apiFactory('/user', userAPI);
apiFactory('/topic', topicAPI);
apiFactory('/prompt', promptAPI);
apiFactory('', {get: {'*': (req, res) => res.json({user: req.user})}}); //api catchall

app.use('/resources', express.static(staticify('/resources'), {index: false}));
app.use('/', express.static(staticify(APPENTRY), {index: false}));
app.get('/:lang/*', ensurelogin, index);
app.get('/:lang', (req, res) => res.redirect(`${req.params.lang}/dashboard`));
app.get('/', (req, res) => res.redirect('/en-US'));

// -- start server
server.listen(PORT, () => console.log(`[SERVER] listening on *:${PORT}`));

//module.exports = { io: io, app: app, db: db };

