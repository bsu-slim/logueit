import './App.css';
import logo from './logueitlogo.svg';
import io from 'socket.io-client';
import React from 'react';
import clsx from 'clsx';
import axios from 'axios';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

import {
  makeStyles,
  createMuiTheme
} from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import List from '@material-ui/core/List';
import ListItemLink from './components/ListItemLink';

import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';

import DashboardIcon from '@material-ui/icons/DashboardRounded';
import SettingsIcon from '@material-ui/icons/SettingsRounded';
import ChatIcon from '@material-ui/icons/ForumRounded';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import defaults from './defaults';
import { T } from './util';

import Dashboard from './views/Dashboard';
import Settings from './views/Settings';
import Chat from './views/Chat';
import Login from './views/Login';
import Register from './views/Register';

import { useVoiceChat } from './hooks/useVoiceChat';

import Theme from './App.theme';
import Styles from './App.styles';

const darkTheme = createMuiTheme(Theme);
const useStyles = makeStyles(Styles);

let lang = 'en-US';
let defaultlang = defaults.langdefault;

function __getUser(user, setUser) {
  let getUser = async () => {
    let response = await axios(`/api/user/get`)
    setUser( response.data || {});
  }
  getUser()
}

function __isAuthed(user) {
  return user && Object.keys(user).length !== 0 && user.constructor === Object;
}

function truncateToLang(the_url) {
  var arr = the_url.split('/');
  let pop = arr.pop();
  if(pop in defaults.langsLookup) {
    return arr.join('/') + '/' + pop
  } else {
    return truncateToLang(arr.join('/'))
  }
}

// connection parameters for signaling server and io
const hostname   = window.location.hostname;
const serverport = window.location.port;
const sockurl = `${hostname}:${serverport}`;  // the current socket URL based on production or not
const socket  = io(sockurl);                  // make a new socket

function App() {
  const classes = useStyles(); // load styles
  const [user, setUser] = React.useState({}); // handle auth state
  const [open, setOpen] = React.useState(false); // handle menu state
  const [anchorEl, setAnchorEl] = React.useState(null); // handle account menu state
  const openEl = Boolean(anchorEl); // calc if the menu shows

  // event handlers for updating component states
  const handleDrawerOpen = () => { setOpen(!open) };
  const handleMenu = (event) => { setAnchorEl(event.currentTarget) };
  const handleClose = () => { setAnchorEl(null) };

  const handleLogout = () => { window.location = `${truncateToLang(window.location.href)}/logout`}

  React.useEffect(() => {
    __getUser(user, setUser);
  }, []);

  const vchat = useVoiceChat(user, socket);

  // handle navigation generation and translation
  const navtext =  [T('Dashboard'),    T('Settings'),    T('Chat')];
  const navlinks = ['dashboard',      'settings',      'chat'];
  const navicons = [<DashboardIcon />, <SettingsIcon />, <ChatIcon />];

  return (
    <ThemeProvider theme={darkTheme}>
      <Router forceRefresh={false /* reload every link click */}>
        <div className={classes.appRoot}>
          <CssBaseline />
          <AppBar
            color="primary"
            position="fixed"
            className={clsx(classes.appBar, {
              [classes.appBarShift]: open,
            })}
          >
            <Toolbar>
              <Typography className={classes.title} variant="h6" noWrap>
                LogueIt
              </Typography>
              { __isAuthed(user) && (
                <div>
                  <IconButton
                    aria-label="account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
                  >
                    <AccountCircle />
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={openEl}
                    onClose={handleClose}
                  >
                    {//<MenuItem onClick={handleClose}>{T('Account')}</MenuItem>
                    }
                    <MenuItem onClick={handleLogout}>{T('Logout')}</MenuItem>
                  </Menu>
                </div>
              )}
            </Toolbar>
          </AppBar>
          <Drawer
            variant="permanent"
            className={clsx(classes.drawer, {
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
              [classes.drawerUserless]: !__isAuthed(user)
            })}
            classes={{
              paper: clsx({
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
                [classes.drawerUserless]: !__isAuthed(user)
              }),
            }}
          >
            <div className={classes.toolbar}>
              <img src={logo} className="App-logo" alt="logo" />
              <Typography
                variant="h6"
                className={classes.menuText}
                noWrap
              >
                {T('Main Menu')}
              </Typography>
            </div>
            <Divider />
            { __isAuthed(user) && (
            <React.Fragment>
              <List>
                {navtext.map((text, index) => (
                  <ListItemLink 
                    button 
                    key={text} 
                    to={`${navlinks[index]}`} 
                    activeClassName="active"
                    icon={navicons[index]}
                    primary={text}
                  />
                ))}
              </List>
              <Divider />
              <div className={classes.toolbar}>
                <Typography 
                  className={clsx("", {
                    [classes.menuText]: open
                  })}
                  noWrap
                >
                {!open ? T("Expand Menu") : T("Collapse Menu")}
                </Typography>
                <IconButton onClick={handleDrawerOpen}>
                  {!open ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                </IconButton>
              </div>
            </React.Fragment>
            )}
          </Drawer>
          <main className={classes.content}>
            <Switch>
              <Route path={`/:lang/login`}>
                <Login useAuthEffect={[__getUser, user, setUser, __isAuthed]} />
              </Route>
              <Route path={`/:lang/register`}>
                <Register useAuthEffect={[__getUser, user, setUser, __isAuthed]} />
              </Route>
              <Route path={`/:lang/settings`}>
                <Settings useAuthEffect={[__getUser, user, setUser, __isAuthed]} />
              </Route>
              <Route path={`/:lang/chat`}>
                <Chat vchat={vchat} socket={socket} user={user} useAuthEffect={[__getUser, user, setUser, __isAuthed]} />
              </Route>
              <Route path={`/:lang/dashboard`}>
                <Dashboard useAuthEffect={[__getUser, user, setUser, __isAuthed]}/>
              </Route>
              <Route path={`/:lang`}>
                <Redirect to={`/${lang}/dashboard`} />
              </Route>
              <Route path={`/`}>
                <Redirect to={`/${defaultlang}`} />
              </Route>
            </Switch>
          </main>
        </div>
      </Router>
    </ThemeProvider>
  );
}

export default App;
