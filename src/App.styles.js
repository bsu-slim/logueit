import {themevars} from './App.theme';

let appStyle = (theme) => ({
  appRoot: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    height: '100%',
    width: '100%',
    fontFamily: '"Roboto", Arial, Helvetica, sans-serif',
  },
  appBar: {
    width: 'calc(100% - 61px)',
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: themevars.drawers.main,
    width: `calc(100% - ${themevars.drawers.main}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  title: {
    flexGrow: 1
  },
  menuText: {
    marginLeft: 12,
    flexGrow: 1
  },
  drawer: {
    width: themevars.drawers.main,
    height: 'calc(100% - 61px)',
    flexShrink: 0,
    whiteSpace: 'nowrap',
    zIndex: 200,
    position: 'relative'
  },
  drawerOpen: {
    width: themevars.drawers.main,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 5,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(7) + 5,
    },
  },
  drawerUserless: {
    height: 65
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    //padding: theme.spacing(3),
    height: 'calc(100% - 64px)',
  },
});

export default appStyle;