import PRIMARY from '@material-ui/core/colors/blue';
import SECONDARY from '@material-ui/core/colors/pink';

let appTheme = {
  palette: {
    type: 'light',
    primary: PRIMARY,
    secondary: SECONDARY
  },
};

export let themevars = {
  drawers: {
    main: 240,
    settings: 200,
    chat: 300,
  },
  prompt: {
    width: 600,
    height: 800
  }
}


export default appTheme;