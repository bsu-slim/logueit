import React from 'react';
import { NavLink } from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tooltip from '@material-ui/core/Tooltip';

function ListItemLink(props) {
  const { icon, primary, to } = props;

  const CustomLink = React.useMemo(
    () =>
      React.forwardRef((linkProps, ref) => (
        <NavLink ref={ref} to={to} {...linkProps} />
      )),
    [to],
  );

  return (
    <li>
      <Tooltip title={primary} placement="right" arrow>
        <ListItem button component={CustomLink}>
            <ListItemIcon>{icon}</ListItemIcon>
          <ListItemText primary={primary} />
        </ListItem>
      </Tooltip>
    </li>
  );
}

export default ListItemLink;