let defaults = {
  "langdefault": "en-US",
  "langs": [
    { id: 0, value: 'en-US', label: 'English', sub: 'US'},
    { id: 1, value: 'ja-JP', label: 'Japanese' },
    { id: 2, value: 'it-IT', label: 'Italian' },
    { id: 3, value: 'es-ES', label: 'Spanish', sub: 'Spain'}
  ],
  "langsLookup": {
    'en-US': 0,
    'ja-JP': 1,
    'it-IT': 2,
    'es-ES': 3
  }
}

export function setLang(id) {
  defaults.langid = id;
}

export default defaults;
