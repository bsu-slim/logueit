import { useState, useEffect } from 'react';
import { v4 as uuidv4, stringify } from 'uuid';
import Peer from 'peerjs';

// connection parameters for signaling server and io
const hostname   = window.location.hostname;

const peerport   = 3001;
const peeropts   = {
  host: hostname,
  port: peerport,
  config: {
    iceServers: [
      {url:'stun:stun.l.google.com:19302'},
      { url: 'turn:numb.viagenie.ca', credential: 'muazkh', username: 'webrtc@live.com'},
    ]
  }
}

export const useVoiceChat = (user, socket) => {

  // == STATE

  const [id]                      = useState(uuidv4());               // a unique id for this client
  const [me, setMe]               = useState(null);                   // my peer for making calls
  const [room, setRoom]           = useState(null);                   // the current room information
  const [calls, setCalls]         = useState({});                     // the active calls
  const [players, setPlayers]     = useState({});                     // the player playing the streams
  const [roomInput, setRoomInput] = useState('');                     // input box value for room

  // == HELPER FUNCTIONS

  const onProvidedRoomChange = (e) => setRoomInput(e.target.value);

  // handle getting mic stream failure
  const getMediaFail = err => console.error(`${err.type}: ${err.message}`);

  // begin a call to a room
  const startCall = () => socket.emit('join', roomInput, id);

  // end the current call
  const endCall = () => socket.emit('leave', room.room, id);

  useEffect(() => {
    console.log('updating players...');
    console.log(players);
  }, [players])

  useEffect(() => {
    console.log('updating calls...');
    console.log(calls);
  }, [calls])

  // == ENTRY - when the react component loads, run effect once
  useEffect(() => {

    let updatedPlayers = {};
    let updatedCalls = {};
    let myroom = null;
    let newme = new Peer(id, peeropts);
    setMe(newme);

    // handle new call media stream playback
    // once a call is received
    newme.on('call', async call => {
  
      // try to call
      let myStream = null;
      try {
        myStream = await navigator.mediaDevices.getUserMedia({audio: true, video: false});
        call.answer(myStream);                 // got mic, send our mic to them
        call.on('stream', theirStream => {              // got the media, handle call stream
  
          let player = document.createElement('audio');       // create audio dom element for playback
          player.srcObject =theirStream;                      // create a url for stream src
          player.onloadedmetadata = (e) => {                  // once the stream has loaded metadata, play it
            console.log('playing back audio from caller...')  // print out
            player.play();                                    // play the audio stream
          }
          updatedPlayers[call.peer] = player;
          setPlayers({...players, ...updatedPlayers});        // update player list
    
        });  // got speakers, handle playback
      } catch(err) {getMediaFail(err);}

      updatedCalls[call.peer] = call;          // add the new call to it
      setCalls({...calls, ...updatedCalls});   // update active calls list
  
    })                        // receiving call, handle it

    socket.on('joined', async roomState => {                // user has joined a room

      console.log('joined room:', roomState);
      let stream = null;
      let outgoing = null;
      try {
        
        stream = await navigator.mediaDevices.getUserMedia( // try mic stream
          {audio: true, video: false}
        );
        myroom = roomState;
        setRoom(myroom);                                    // were calling the room so set current room were in
        let callers = Object.keys(roomState.callers);       // get the callers
        for(let i = 0; i < callers.length; i++) {           // for each caller, call
          let caller = callers[i];                          // current caller
          if(id !== caller) {
            outgoing = newme.call(caller, stream)                // call current caller id with the stream if not self
            outgoing.on('stream', theirStream => {               // got the media, handle call stream
  
              let player = document.createElement('audio');      // create audio dom element for playback
              player.srcObject =theirStream;                     // create a url for stream src
              player.onloadedmetadata = (e) => {                 // once the stream has loaded metadata, play it
                console.log('playing back audio from caller...') // print out
                player.play();                                   // play the audio stream
              }

              updatedPlayers[outgoing.peer] = player;
              setPlayers({...players, ...updatedPlayers});       // update player list

            });
          }
        }
      } catch (err) { getMediaFail(err) }                        // if the media failed, handle it

    });

    // someone is joining the room
    socket.on('joining', uid => {
      console.log(`${uid} has joined the room...`);
    });

    // someone is leaving the room
    socket.on('leaving', uid => {
      let updatedPlayers = players;
      delete updatedPlayers[uid];
      setPlayers(updatedPlayers);
      console.log(`${uid} has left the room...`);
    });

    // user has inspected a room
    socket.on('inspected', roomState => console.log('got room:', roomState));

    // user has left the room
    socket.on('left', roomState => {
      console.log('left room:', roomState.room);
      let activeCalls = Object.keys(updatedCalls);      // get list of active calls
      console.log(activeCalls);
      for (let i = 0; i < activeCalls.length; i++) {    // for every call
        console.log('ending call with', activeCalls[i]);
        let call = calls[activeCalls[i]];               // get the call
        call.close();                                   // close the call
      }
      setCalls({});                                     // reset the calls

      let activePlayers = Object.keys(players);         // list of active players
      for (let i = 0; i < activePlayers.length; i ++) { // for every player
        let player = players[activePlayers[i]];         // get the player
        player.pause();                                 // pause the playback
      }
      setPlayers({});                                   // clear out the old players
      myroom = null;
      setRoom(myroom);
    });

    socket.on('user-disconnected', (uid) => {
      updatedPlayers = players;
      delete updatedPlayers[uid];
      setPlayers(updatedPlayers);
      console.log(`${uid} has disconnected from the room...`);
    });

    return () => {
      if(me) me.destroy();         // destroy my own peer
      socket.off('joined');
      socket.off('joining');
      socket.off('leaving');
      socket.off('inspect');
      socket.off('left');
    }
  }, [room]);



  return {id, me, room, calls, players, roomInput, onProvidedRoomChange, startCall, endCall}
}