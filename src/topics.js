let topics = [
  {id: 0, label: "Landscapes", value: "landscapes"},
  {id: 1, label: "Cars", value: "cars"},
  {id: 2, label: "People", value: "people"},
  {id: 3, label: "Places", value: "places"},
  {id: 4, label: "Animals", value: "animals"},
  {id: 5, label: "Furniture", value: "furniture"}
];

export default topics;