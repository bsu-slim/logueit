let i18nl10n = {
  "Dashboard": {
    "en-US": "Dashboard",
    "it-IT": "Pannello di Controllo",
    "ja-JP": "ダッシュボード",
    "es-ES": "Tablero"
  },
  "Recording": {
    "en-US": "Recording",
    "it-IT": "Registrazione Dati",
    "ja-JP": "録音",
    "es-ES": "Grabación de los datos"
  },
  "Select Recording Features": {
    "en-US": "Select Recording Features",
    "it-IT": "Seleziona quali dati registrare",
    "ja-JP": "録音機能を選択する",
    "es-ES": "Seleccione qué datos grabar"
  },
  "Click Gestures": {
    "en-US": "Click Gestures",
    "it-IT": "Movimenti del mouse",
    "ja-JP": "クリックジェスチャ",
    "es-ES": "Movimientos del ratón"
  },
  "Text Chat": {
    "en-US": "Text Chat",
    "it-IT": "Conversazione in Chat",
    "ja-JP": "テキストチャット",
    "es-ES": "Conversación en Chat"
  },
  "Select...": {
    "en-US": "Select...",
    "it-IT": "Seleziona...",
    "ja-JP": "選択する...",
    "es-ES": "Seleccione..."
  },
  "Voice Chat": {
    "en-US": "Voice",
    "it-IT": "Conversazione Orale",
    "ja-JP": "ボイスチャット",
    "es-ES": "Conversación Oral"
  },
  "Start Recording": {
    "en-US": "Start Recording",
    "it-IT": "Inizia La Registrazione",
    "ja-JP": "録音を開始",
    "es-ES": "Empieza La Registración"
  },
  "Search": {
    "en-US": "Search",
    "it-IT": "Ricerca",
    "ja-JP": "検索",
    "es-ES": "Buscar"
  },
  "Settings": {
    "en-US": "Settings",
    "it-IT": "Impostazioni",
    "ja-JP": "設定",
    "es-ES": "Configuraciónes"
  },
  "Language": {
    "en-US": "Language",
    "it-IT": "Lingua",
    "ja-JP": "言語",
    "es-ES": "Idioma"
  },
  "Japanese": {
    "en-US": "Japanese",
    "it-IT": "Giapponese",
    "ja-JP": "日本語",
    "es-ES": "Japonés"
  },
  "English": {
    "en-US": "English",
    "it-IT": "Inglese",
    "ja-JP": "英語",
    "es-ES": "Inglés"
  },
  "Italian": {
    "en-US": "Italian",
    "it-IT": "Italiano",
    "ja-JP": "イタリア語",
    "es-ES": "Italiano"
  },
  "Spanish": {
    "en-US": "Spanish",
    "it-IT": "Spagnolo",
    "ja-JP": "スペイン語",
    "es-ES": "Español"
  },
  "Chat": {
    "en-US": "Chat",
    "it-IT": "Chat",
    "ja-JP": "チャット",
    "es-ES": "Chat"
  },
  "Landscapes": {
    "en-US": "Landscapes",
    "it-IT": "Paesaggi",
    "ja-JP": "風景",
    "en-EN": "Paisajes"
  },
  "Cars": {
    "en-US": "Cars",
    "it-IT": "Veicoli",
    "ja-JP": "車",
    "en-EN": "Carros"
  },
  "People": {
    "en-US": "People",
    "it-IT": "Persone",
    "ja-JP": "人々",
    "en-EN": "Personas"
  },
  "Places": {
    "en-US": "Places",
    "it-IT": "Luoghi",
    "ja-JP": "場所",
    "en-EN": "Lugares"
  },
  "Animals": {
    "en-US": "Animals",
    "it-IT": "Animali",
    "ja-JP": "動物",
    "en-EN": "Animales"
  },
  "Furniture": {
    "en-US": "Furniture",
    "it-IT": "Mobilia",
    "ja-JP": "家具",
    "en-EN": "Muebles"
  },
  "Selected Topic": {
    "en-US": "Selected Topic",
    "it-IT": "Argomenti Selezionati",
    "ja-JP": "現在の話題",
    "en-EN": "Argumento Seleccionado"
  },
  "Please select a topic": {
    "en-US": "Selected Topic",
    "it-IT": "Seleziona un argomento, per favore",
    "ja-JP": "話題を選択してください",
    "en-EN": "Seleccione un argumento, por favor"
  },
  "Logout": {
    "en-US": "Logout",
    "it-IT": "Disconnetti",
    "ja-JP": "ログアウト",
    "en-EN": "Cerra la sesión"
  }, 
  "System": {
    "en-US": "System",
    "it-IT": "Sistema",
    "ja-JP": "システム",
    "en-EN": "Sistema"
  },
  "Account": {
    "en-US": "Account",
    "it-IT": "Account",
    "ja-JP": "アカウント",
    "en-EN": "Account"
  },
  "My native language is": {
    "en-US": "My native language is",
    "it-IT": "La mia lingue nativa é",
    "ja-JP": "私の母国語は",
    "en-EN": "Mi idioma nativo es"
  },
  "My target language is": {
    "en-US": "My target language is",
    "it-IT": "La lingua che sto imparando é",
    "ja-JP": "私の目標言語は",
    "en-EN": "El idioma que estoy aprendiendo es"
  },
  "Want to discuss": {
    "en-US": "Want to discuss",
    "it-IT": "Vorrei parlare di",
    "ja-JP": "話し合いたい",
    "en-EN": "Me gustaría hablar de"
  },
  "Email": {
    "en-US": "Email",
    "it-IT": "",
    "ja-JP": "Eメール",
    "en-EN": ""
  },
  "Password": {
    "en-US": "Password",
    "it-IT": "",
    "ja-JP": "パスワード",
    "en-EN": ""
  },
  "Register": {
    "en-US": "Register",
    "it-IT": "",
    "ja-JP": "登録",
    "en-EN": ""
  },
  "Login": {
    "en-US": "Login",
    "it-IT": "",
    "ja-JP": "ログイン",
    "en-EN": ""
  },
  "Main Menu": {
    "en-US": "Main Menu",
    "it-IT": "",
    "ja-JP": "メインメニュー",
    "en-EN": ""
  },
  "Expand Menu": {
    "en-US": "Expand Menu",
    "it-IT": "",
    "ja-JP": "メニューを展開",
    "en-EN": ""
  },
  "Chat Message": {
    "en-US": "Chat Message",
    "it-IT": "",
    "ja-JP": "チャットメッセージ",
    "en-EN": ""
  }
}
export default i18nl10n;
