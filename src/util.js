import i18nl10n from './translations'; // loading in the translation values for the app to use
import defaults from './defaults';

// capitalize first letter function shim
function capitalizeFirstLetter(s) {
  return s.charAt(0).toUpperCase() + s.slice(1);
}

// Translation code handling
function T(string) {
  let lang = window.location.pathname.split('/')[1];
  let s = string;
  let translations = i18nl10n[s];
  let translated = undefined;
  if(translations) { // if translations exist
    translated = translations[lang]; //attempt to get translation
  }
  if(translated) { // if translated successfully
    s = translated; // replace with translation
  } else { // failed to translate, warn user
    console.warn(`Translation in localization "${lang}" missing for string "${s}". defaulting...`)
  }
  return s // return translation, or the original if none exists
}

function validateLanguage(history) {
  let lang = window.location.pathname.split('/')[1];
  let lookup = defaults.langsLookup[lang];
  if(lookup === undefined) { history.push('/en-US') } // if bad language, reset
}

function TSelect(list, showValue) {
  let newList = [];
  for(let i = 0; i < list.length; i++) {
    let l = Object.assign({}, list[i]);
    l.label = T(l.label) 
    if(showValue) l.label += ` (${l.value})`;
    newList.push(l)
  }
  return newList;
}

const getAudioContext = () => {
  AudioContext = window.AudioContext || window.webkitAudioContext;
  const audioContext = new AudioContext();
  const analyser = audioContext.createAnalyser();
  return {audioContext, analyser}
}

export {
  validateLanguage,
  capitalizeFirstLetter,
  T,
  TSelect
};