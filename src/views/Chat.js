import React, { useState, useEffect } from "react";
import io from "socket.io-client";
import Select from 'react-select';
//import { v4 as uuidv4 } from 'uuid';
import { T, TSelect, validateLanguage } from '../util';
import { useHistory, useParams } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import { themevars } from '../App.theme';

import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';

import SendIcon from '@material-ui/icons/SendRounded';
import SearchIcon from '@material-ui/icons/SearchRounded';
import RecordIcon from '@material-ui/icons/FiberManualRecordRounded';
import PersonIcon from '@material-ui/icons/Person';

//import defaults from '../defaults';
//import topiclist from '../topics';


import Styles from './Chat.styles';

const useStyles = makeStyles(Styles);

function Chat({user, socket, vchat}) {
  const history = useHistory();
  const {lang} = useParams();
  const classes = useStyles();
  const [messages, setMessages] = useState([]);
  const [state, setState] = useState({click: true, text: true, voice: false});

  // custom voice chat hook
  const {
    id,
    room,
    players,
    roomInput,
    onProvidedRoomChange,
    startCall,
    endCall
   } = vchat;

   // when I join the call...
   let joinCall = () => startCall(roomInput);

   // when I leave the call
   let leaveCall = () => {endCall(); setMessages([])};

  // if shift entering, allow box to make a new line, else send message
  let handleMessageKeys = (ev) => {
    if(ev.key === 'Enter' && ev.shiftKey) return;
    if(ev.key === 'Enter') { ev.preventDefault(); sendMessage();}
  }

  // if enter pressed on join input
  let handleJoinKeys = (e) => { if(e.key === 'Enter') joinCall(); }

  // when I send a message...
  let sendMessage = () => {
    let m = document.getElementById('message');
    if(socket) socket.emit('message', m.value, room.room);
    m.value = '';
    m.focus();
  }

  // when I gesture...
  let handleGesture = (e) => {
    if(room) {
      var rect = e.target.getBoundingClientRect();
      var x = e.clientX - rect.left; //x position within the element.
      var y = e.clientY - rect.top;  //y position within the element.
      socket.emit('gesture', room.room, x, y);
      clickEffect(x, y);
    }
  }

  // effect to spawn click ping
  let clickEffect = (x, y, them) => {
    var d=document.createElement("div");
    var rect = document.getElementById('prompt-image').getBoundingClientRect();
    if(!them) d.className="clickEffect";
    else d.className="clickEffectThem";
    d.style.top=(y+rect.top)+"px";
    d.style.left=(x+rect.left)+"px";
    document.getElementById('prompt-image').appendChild(d);
    d.addEventListener('animationend',function(){d.parentElement.removeChild(d);}.bind(this));
  }

  // record modes toggle handling
  const handleChange = (event) => setState({ ...state, [event.target.name]: event.target.checked });

  // set up socket config (componentdidmount)
  useEffect(() => {
    socket.on('message', (message) => setMessages((messages) => [...messages, message]));
    socket.on('gesture', (x, y) => { clickEffect(x, y, true) });
    let j = document.getElementById('join');
    j.focus();
    return () => {
      socket.off('message');
      socket.off('gesture');
    }
  }, []);

  // make sure URL has a valid language, if not, redirect to one
  validateLanguage(history);

  // list of chatters in the room
  let chatters = room ? Object.keys(players) : [];

  console.log(room);
  console.log(lang);

  return (
    <div className="App-view" id="chat">
      <Box className={classes.drawer}>
        <Typography className={classes.title} variant="h6">{T('Chat')}</Typography>
        <Divider className={classes.divider} />
        <Typography className={classes.subtitle}>{T('Current Room')}</Typography>
        <div className={classes.drawerform}>
          <TextField
            fullWidth
            id="join"
            variant="outlined"
            label="Room"
            size="small"
            value={roomInput}
            onKeyPress={handleJoinKeys}
            onChange={onProvidedRoomChange}
            disabled={room ? true : false}
          />
          <Button
            className={classes.join}
            disabled={roomInput === '' ? true : false}
            fullWidth
            variant="contained"
            color={room ? "secondary":"primary"}
            onClick={room ? leaveCall:joinCall}
          >
            { room ? T('Leave Room') : T('Join Room and Record')}
          </Button>
        </div>
        <Divider className={classes.divider} />
        {room 
          ?<Card className={classes.user}>
            <CardHeader 
              avatar={
                <Avatar aria-label="user in room">
                  <PersonIcon />
                </Avatar>
              }
              title="Me"
              subheader={id}
              subheaderTypographyProps={{className: classes.username}}
              titleTypographyProps={{className: classes.username}}
            />
          </Card>
          : ''
        }
        {chatters.map((chatter)=> 
          <Card key={chatter} className={classes.user}>
            <CardHeader 
              avatar={
                <Avatar aria-label="user in room">
                  <PersonIcon />
                </Avatar>
              }
              title="User"
              subheader={chatter}
              subheaderTypographyProps={{className: classes.username}}
              titleTypographyProps={{className: classes.username}}
            />
          </Card>)}
        <Box className={classes.expander} />
        {/*<Divider className={classes.divider} />
        <Typography className={classes.subtitle} >{T('Recording')}</Typography>
        <form className={classes.drawerform} onSubmit="">
          <FormControl className={classes.switches} component="fieldset">
            <FormLabel component="legend">{T('Select Recording Features')}</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={<Switch checked={state.click} onChange={handleChange} name="click" />}
                label={T("Click Gestures")}
              />
              <FormControlLabel
                control={<Switch checked={state.text} onChange={handleChange} name="text" />}
                label={T("Text Chat")}
              />
              <FormControlLabel
                control={<Switch checked={state.voice} onChange={handleChange} name="voice" disabled />}
                label={T("Voice Chat")}
              />
            </FormGroup>
          </FormControl>
          <Button
            variant="contained"
            color="primary"
            startIcon={<RecordIcon />}
          >
            {T('Start Recording')}
          </Button>
        </form>
            <Divider />*/}
      </Box>
      <Box className={classes.content}>
        <Box className={classes.chat}>
          <Box className={classes.log}>
            {messages.map((message, index) => (
              <Paper
                className={message.who === 'me' ? classes.me : classes.them}
                elevation={3}
                variant="outlined"
                key={index}
              >
                <Typography>{`${message.from.name.first}: ${message.message}`}</Typography>
              </Paper>
            ))}
          </Box>
          <Box className={classes.inputbar}>
            <form className={classes.inputform}>
              <TextField
                id="message"
                onKeyPress={handleMessageKeys}
                label={room ? T("Chat Message") : T('To Chat, Join a Room')}
                variant="outlined"
                multiline
                rowsMax={3}
                className={classes.input}
                color="secondary"
                disabled={room ? false:true}
              />
              <IconButton
                color="secondary"
                onClick={sendMessage}
                disabled={room ? false:true}
              >
                <SendIcon />
              </IconButton>
            </form>
          </Box>
        </Box>
        {room
            ? 
            <Box className={classes.prompt}>
              <Typography className={classes.title} variant="h4">
                { (room.prompt[lang] && room.prompt[lang] !== "") 
                    ? room.prompt[lang]
                    : room.prompt['en-US']
                }
              </Typography>
              <div id="prompt-image">
                <img 
                  className={classes.promptImg}
                  onClick={handleGesture}
                  draggable="false"
                  src={`https://source.unsplash.com/${themevars.prompt.width}x${themevars.prompt.height}/?${room.topic.topic}`} 
                />
              </div>
            </Box>
            :
            <Box className={classes.prompt}>
              <Typography className={classes.title} variant="h4">Join a room to discuss something!</Typography>
            </Box>
        }
        
      </Box>
    </div>
  );

}

export default Chat;
