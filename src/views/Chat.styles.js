import {themevars} from '../App.theme';
import { Hidden } from '@material-ui/core';

let chatStyle = (theme) => ({
  drawer: {
    width: themevars.drawers.chat,
    backgroundColor: theme.palette.background.paper,
    borderRightWidth: '1px',
    borderRightStyle: 'solid',
    borderRightColor: theme.palette.divider,
    height: '100%',
    whiteSpace: 'nowrap',
    zIndex: 100,
    position: 'relative',
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    overflowY: "auto"
  },
  drawerform: {
    width: '100%',
    padding: theme.spacing(0, 3, 2, 3),
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  title: {
    boxSizing: 'border-box',
    padding: 24,
    textAlign: 'center'
  },
  subtitle: {
    fontWeight: 'bold',
    boxSizing: 'border-box',
    padding: theme.spacing(2, 3),
    color: theme.palette.text.secondary
  },
  switches: {
    padding: theme.spacing(2, 0),
  },
  expander: {
    flexGrow: 1,
    height: 'auto'
  },
  divider: {
    width: '100%',
  },
  chat: {
    width: '40%',
    height: '100%',
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'center',
    justifyContent: 'space-between',
    resize: 'horizontal'
  },
  me: {
    maxWidth: '80%',
    margin: '0 0 16px 20%',
    boxSizing: 'border-box',
    padding: theme.spacing(2, 3),
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText
  },
  them: {
    maxWidth: '80%',
    margin: '0 0 16px 0',
    boxSizing: 'border-box',
    padding: theme.spacing(2, 3)
  },
  prompt: {
    width: '60%',
    height: '100%',
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: theme.palette.background.default,
    overflow: 'auto'
  },
  promptImg: {
    borderRadius: 8,
    webkitUserDrag: 'none',
    overflow: 'hidden'
  },
  inputbar: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    borderTopWidth: '1px',
    borderRightWidth: '1px',
    borderLeftWidth: '0',
    borderBottomWidth: '0',
    borderStyle: 'solid',
    borderColor: theme.palette.divider,
    boxSizing: 'border-box',
  },
  inputform: {
    width: '100%',
    padding: 8,
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  input: {
    width: 'calc(100% - 48px)',
    margin: '0 12px 0 0',
  },
  content: {
    flexGrow: 1,
    width: `calc(100% - ${themevars.drawers.chat}px)`,
    height: 'calc(100% - 1px)',
    backgroundColor: theme.palette.background.default,
    borderRightWidth: '1px',
    borderRightStyle: 'solid',
    borderRightColor: theme.palette.divider,
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  log: {
    flexGrow: 1,
    width: '100%',
    borderTopWidth: '1px',
    borderTopStyle: 'solid',
    borderTopColor: theme.palette.divider,
    height: '100%',
    overflowY: 'scroll',
    padding: theme.spacing(3, 3)
  },
  formControl: {
    margin: theme.spacing(0),
    minWidth: '100%',
    boxSizing: 'border-box',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  join: {
    marginTop: theme.spacing(2)
  },
  user: {
    width: '100%',
    marginTop: theme.spacing(1),
    overflow: 'hidden'
  },
  username: {
    width: 'calc(100% - 52px)',
    display: 'block',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  }
})

export default chatStyle;