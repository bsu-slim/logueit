import './Dashboard.css';
import { T, validateLanguage } from '../util';
import { useHistory } from "react-router-dom";
import React, { useState, useEffect } from "react";
import socketIOClient from "socket.io-client";

const ENDPOINT = "http://localhost:3000";

function Dashboard(props) {
  
  const {useAuthEffect} = props; 
  useEffect(() => {
    useAuthEffect[0](useAuthEffect[1], useAuthEffect[2]);
  }, []);
  

  const history = useHistory();
  validateLanguage(history);

  const [responses, setResponses] = useState([]);

  /*useEffect(
    () => { // tell react state is changing to rerender and trigger events
      const socket = socketIOClient(ENDPOINT); // connect client to server
      socket.on("FromAPI", data => { // socket receives message "FromAPI" with payload data
        setResponses((responses) => [...responses, data]); // update the react state
      });
    }, 
    []
  );*/

  // for each response, create an html element and put it in an array
  let elements = responses.map((response, index) => (
    <h2 key={response} style={{width: '100%'}}> 
      It's <time dateTime={response}>{response}</time>
    </h2>
  ));

  return (
    <div className="App-view" id="dashboard">
      {elements}
    </div>
  );
  
}

export default Dashboard;