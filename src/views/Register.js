import React from 'react';
import { useParams, useHistory } from "react-router-dom";
import { T, validateLanguage } from '../util';

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';

import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';
import ArrowBackIosOutlinedIcon from '@material-ui/icons/ArrowBackIosOutlined';

import Zoom from '@material-ui/core/Zoom';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing(3)
  },
  error: {
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.error.contrastText,
    backgroundColor: theme.palette.error.dark
  },
  form: {
    textAlign: 'right'
  },
  input: {
    marginBottom: theme.spacing(3)
  },
  button: {
  }
}));

function Register() {
  const classes = useStyles();
  const { lang } = useParams();
  const history = useHistory();
  validateLanguage(history);
  const urlParams = new URLSearchParams(window.location.search);
  const info = urlParams.get('info');

  return (
    <Zoom in={true}>
      <Container maxWidth="xs" className={classes.root}>
        <Grid className={classes.root} container spacing={3}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography
                variant="h2"
                color="secondary"
                gutterBottom
              >
                {T('Register')}
              </Typography>
              <form 
                className={classes.form}
                method="POST"
                action={`/${lang}/register`}
              >
                <TextField
                  id="fname"
                  name="fname"
                  size="small"
                  type="text"
                  label={T('First Name')}
                  variant="outlined"
                  className={classes.input}
                  fullWidth
                  required
                />
                <TextField
                  id="lname"
                  name="lname"
                  size="small"
                  type="text"
                  label={T('Last Name')}
                  variant="outlined"
                  className={classes.input}
                  fullWidth
                  required
                />
                <TextField
                  id="email"
                  name="email"
                  size="small"
                  type="email"
                  label={T('Email')}
                  variant="outlined"
                  className={classes.input}
                  fullWidth
                  required
                />
                <TextField
                  id="password"
                  name="password"
                  size="small"
                  type="password"
                  label={T('Password')}
                  variant="outlined"
                  className={classes.input}
                  fullWidth
                  required
                />
                <TextField
                  id="passwordVerify"
                  name="passwordVerify"
                  size="small"
                  type="password"
                  label={T('Verify Password')}
                  variant="outlined"
                  helperText={T('* required field')}
                  className={classes.input}
                  fullWidth
                  required
                />
                <ButtonGroup
                  variant="contained"
                  aria-label="contained button group"
                  color="secondary"
                  disableElevation
                >
                <Button
                    href={`/${lang}/login`}
                    variant="outlined"
                    color="secondary"
                    className={classes.button}
                    startIcon={<ArrowBackIosOutlinedIcon />}
                  >
                    {T('Return to Login')}
                  </Button>
                  <Button
                    type="submit"
                    variant="contained"
                    color="secondary"
                    className={classes.button}
                    endIcon={<ExitToAppOutlinedIcon />}
                  >
                    {T('Create Account')}
                  </Button>
                </ButtonGroup>
              </form>
            </Paper>
            { info ? 
            (<Paper className={classes.error}>
              <Typography variant="body2">{info}</Typography>
            </Paper>) : ''
            }
          </Grid>
        </Grid>
      </Container>
    </Zoom>
  );
}

export default Register;