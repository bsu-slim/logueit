import React from 'react';
import Select from 'react-select';
import { T, TSelect, validateLanguage } from '../util';
import { useHistory } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';

import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import WarningIcon from '@material-ui/icons/WarningRounded';

import defaults from '../defaults';
import { setLang } from '../defaults';

import Style from './Settings.styles';

const useStyles = makeStyles(Style);

function Settings() {
  
  const history = useHistory();
  const classes = useStyles();
  const [tabval, setTabVal] = React.useState(0);

  const handleTabChange = (event, newValue) => {
    setTabVal(newValue);
  };

  function a11yProps(index) {
    return {
      id: `vertical-tab-${index}`,
      'aria-controls': `vertical-tabpanel-${index}`,
    };
  }

  validateLanguage(history);

  let lang = window.location.pathname.split('/')[1];
  let lookup = defaults.langsLookup[lang];

  // translate language values for ui
  let langs = TSelect(defaults.langs, true);

  let tsystem = T('System')
  let taccount = T('Account')

  return (
    <div className="App-view" id="settings">
      <Box className={classes.drawer}>
        <Typography className={classes.title} variant="h6">{T('Settings')}</Typography>
        <Divider />
        <Tabs
        orientation="vertical"
        variant="scrollable"
        value={tabval}
        onChange={handleTabChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        <Tab label={tsystem} {...a11yProps(0)} />
        <Tab label={taccount} {...a11yProps(1)} />
      </Tabs>
      </Box>
      <Box className={classes.content}>
      <TabPanel className={classes.panel} value={tabval} index={0}>
        <Typography variant="h6">{tsystem}</Typography>
        <form>
          <div className="input-group">
            <label>{T('Language')}</label>
            <Select
              options={langs}
              defaultValue={langs[lookup]}
              onChange={(e) => {
                setLang(e.id);
                history.push(`/${e.value}/settings`);
                window.location.reload();
              }}
            />
            <Typography 
              variant="caption"
              color="secondary"
              className={classes.warning}
              >
              <WarningIcon color="secondar" />
              {T('Warning: will disconnect active calls if changed')}
            </Typography>
          </div>
        </form>
      </TabPanel>
      <TabPanel className={classes.panel} value={tabval} index={1}>
      <Typography variant="h6">{taccount}</Typography>
      </TabPanel>
      </Box>
    </div>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

export default Settings;