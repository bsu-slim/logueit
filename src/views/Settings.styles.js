import {themevars} from '../App.theme';

let settingsStyle = (theme) => ({
 
  drawer: {
    width: themevars.drawers.settings,
    backgroundColor: theme.palette.background.paper,
    borderRightWidth: '1px',
    borderRightStyle: 'solid',
    borderRightColor: theme.palette.divider,
    height: '100%',
    flexShrink: 0,
    whiteSpace: 'nowrap',
    zIndex: 100,
    position: 'relative',
  },
  title: {
    boxSizing: 'border-box',
    padding: 24
  },
  panel: {
    width: '100%',
  },
  content: {
    flexGrow: 1,
    width: `calc(100% - ${themevars.drawers.settings}px)`,
    height: 'calc(100% - 1px)',
    backgroundColor: theme.palette.background.default,
    borderRightWidth: '1px',
    borderRightStyle: 'solid',
    borderRightColor: theme.palette.divider,
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  warning: {
    display: 'flex',
    alignItems: 'flex-end',
    fontWeight: 'bold'
  }

});

export default settingsStyle;